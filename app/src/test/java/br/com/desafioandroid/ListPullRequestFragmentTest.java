package br.com.desafioandroid;

import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toolbar;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import br.com.desafioandroid.view.acitivity.MainActivity;
import br.com.desafioandroid.view.fragment.AbstractFragment;
import br.com.desafioandroid.view.fragment.ContentFragment;
import br.com.desafioandroid.view.fragment.ListPullResquestFragment;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.robolectric.util.FragmentTestUtil.startFragment;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(packageName = "br.com.desafioandroid", constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class ListPullRequestFragmentTest {


    private ListPullResquestFragment fragment;
    private MainActivity activity;

    @Before
    public void setUp() throws Exception{
        fragment = ListPullResquestFragment.newInstance();
    }

    @Test
    public void shouldNotBeNull() throws Exception{
        startFragment(fragment);
        assertNotNull(fragment);
    }

}
