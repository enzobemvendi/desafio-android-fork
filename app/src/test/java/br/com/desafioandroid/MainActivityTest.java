package br.com.desafioandroid;

import android.os.Build;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import br.com.desafioandroid.view.acitivity.MainActivity;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(packageName = "br.com.desafioandroid", constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class MainActivityTest {

    private MainActivity activity;

    @Before
    public void setUp() throws Exception{
        //activity = Robolectric.setupActivity(MainActivity.class);
        activity = Robolectric.buildActivity( MainActivity.class )
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception{
        assertNotNull(activity);
    }

    @Test
    public void shouldHaveWelcomeContentFragment() throws Exception{
        assertNotNull(activity.getFragmentManager().findFragmentByTag("content"));
    }


}
