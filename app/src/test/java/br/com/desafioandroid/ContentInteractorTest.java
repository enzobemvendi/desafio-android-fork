package br.com.desafioandroid;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroid.helper.facade.DesafioBO;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.Repositories;
import br.com.desafioandroid.presenter.content.OnContentCallback;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ContentInteractorTest extends TestCase {

    List<Item> itemList;
    OnContentCallback contentCallback;
    public void testReturnListProduct(){
        itemList               = new ArrayList<Item>();

        RestAdapter restAdapter = DesafioBO.getRestAdapter();
        restAdapter.create(RestApi.class).getRepositories(1, new Callback<Repositories>() {
            @Override
            public void success(Repositories repositories, Response response) {

                for (Item item : repositories.getItems()){
                    itemList.add(item);
                }
                assertNotNull(itemList);
                boolean result = contentCallback.resultList(itemList);
                assertTrue(result);

            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.error("error " + error);
            }
        });
    }
}
