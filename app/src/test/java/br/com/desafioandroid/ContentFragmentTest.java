package br.com.desafioandroid;

import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import br.com.desafioandroid.view.acitivity.MainActivity;
import br.com.desafioandroid.view.fragment.ContentFragment;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.robolectric.util.FragmentTestUtil.startFragment;
import static org.junit.Assert.assertThat;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(packageName = "br.com.desafioandroid", constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class ContentFragmentTest {

    private ContentFragment fragment;

    @Before
    public void setUp() throws Exception{
        fragment = ContentFragment.newInstance();
    }

    @Test
    public void shouldNotBeNull() throws Exception{
        startFragment(fragment);
        assertNotNull(fragment);
    }

    @Test
    public void showTitleInTheTopBar() throws Exception {
        String title = new ContentFragment().getResources().getString(
                R.string.title);
        assertThat(title, equalTo("GitHub JavaPop"));
    }
}
