package br.com.desafioandroid;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroid.helper.facade.DesafioBO;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.PullResquest;
import br.com.desafioandroid.model.pojo.Repositories;
import br.com.desafioandroid.presenter.content.OnContentCallback;
import br.com.desafioandroid.presenter.pullrequest.OnListPullRequestCallback;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ListPullRequestInteractorTest extends TestCase {

    List<PullResquest> listPullRequest;
    OnListPullRequestCallback callback;
    public void testReturnListProduct(){
        listPullRequest               = new ArrayList<PullResquest>();

        RestAdapter restAdapter = DesafioBO.getRestAdapter();
        restAdapter.create(RestApi.class).getPullResquest("spring-projects", "spring-boot", new Callback<List<PullResquest>>() {
            @Override
            public void success(List<PullResquest> pullResquests, Response response) {
                for (PullResquest pullResquest : pullResquests) {
                    listPullRequest.add(pullResquest);
                }

                assertNotNull(listPullRequest);
                boolean result = callback.returnListPullRequest(listPullRequest);
                assertTrue(result);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.error("error " + error.getMessage());
            }
        });
    }
}
