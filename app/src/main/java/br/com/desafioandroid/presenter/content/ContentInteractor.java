package br.com.desafioandroid.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroid.helper.facade.DesafioBO;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.Repositories;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ContentInteractor implements OnContentInteractor {
    private List<Item>          itemList;

    /**
     * method to show list of the item
     * */
    @Override
    public void callContent(final OnContentCallback callback,int page) {

        itemList                = new ArrayList<Item>();
        RestAdapter restAdapter = DesafioBO.getRestAdapter();
        restAdapter.create(RestApi.class).getRepositories(page, new Callback<Repositories>() {
            @Override
            public void success(Repositories repositories, Response response) {

                for (Item item : repositories.getItems()){
                    itemList.add(item);
                }
                callback.resultList(itemList);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.error("error " + error);
            }
        });

    }
}
