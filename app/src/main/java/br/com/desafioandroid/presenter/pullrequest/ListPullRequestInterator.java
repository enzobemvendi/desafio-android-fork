package br.com.desafioandroid.presenter.pullrequest;

import android.support.v4.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroid.helper.facade.DesafioBO;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.model.pojo.PullResquest;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ListPullRequestInterator implements OnListPullRequestInteractor {

    private List<PullResquest> listPullRequest;

    /**
     * method to show the list of the PullRequest
     */
    @Override
    public void callPullRequeste(final OnListPullRequestCallback callback, final String user, final String repository) {

        WrapperLog.info("userInteractor " + user + " repository " + repository);
        listPullRequest = new ArrayList<PullResquest>();
        RestAdapter restAdapter = DesafioBO.getRestAdapter();
        restAdapter.create(RestApi.class).getPullResquest(user, repository, new Callback<List<PullResquest>>() {
            @Override
            public void success(List<PullResquest> pullResquests, Response response) {
                for (PullResquest pullResquest : pullResquests) {
                    listPullRequest.add(pullResquest);
                }

                callback.returnListPullRequest(listPullRequest);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.error("error " + error.getMessage());
            }
        });

    }
}
