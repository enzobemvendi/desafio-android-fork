package br.com.desafioandroid.presenter.pullrequest;

import java.util.List;

import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public interface OnListPullRequestCallback {
    boolean returnListPullRequest(List<PullResquest> listPullRequest);
}
