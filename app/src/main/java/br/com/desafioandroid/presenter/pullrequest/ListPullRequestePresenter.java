package br.com.desafioandroid.presenter.pullrequest;

import java.util.List;

import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ListPullRequestePresenter implements OnListPullRequestPresenter, OnListPullRequestCallback {

    private OnListPullRequestView onListPullRequestView;
    private OnListPullRequestInteractor onListPullRequestInteractor;

    /**
     * method to show of the list pullrequest
     */
    public ListPullRequestePresenter(OnListPullRequestView onListPullRequestView) {
        this.onListPullRequestView = onListPullRequestView;
        this.onListPullRequestInteractor = new ListPullRequestInterator();
    }

    /**
     * method to call the interactor
     */
    @Override
    public void callPullRequest(String user, String repository) {
        onListPullRequestInteractor.callPullRequeste(this, user, repository);

    }

    /**
     * method to return list pullrequest
     */
    @Override
    public boolean returnListPullRequest(List<PullResquest> listPullRequest) {
        onListPullRequestView.returnListPullRequest(listPullRequest);
        return true;
    }
}
