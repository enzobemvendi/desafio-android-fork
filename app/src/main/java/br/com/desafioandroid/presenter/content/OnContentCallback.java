package br.com.desafioandroid.presenter.content;

import java.util.List;

import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public interface OnContentCallback {

    public boolean resultList(List<Item> itemList);
}
