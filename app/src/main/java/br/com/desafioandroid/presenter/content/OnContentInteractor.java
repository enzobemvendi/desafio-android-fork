package br.com.desafioandroid.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public interface OnContentInteractor {
    void callContent(OnContentCallback callback, int page);
}
