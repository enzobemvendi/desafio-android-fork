package br.com.desafioandroid.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public interface OnContentView {

    public void callContent(int page);
    public void resultList(List<Item> itemList);
}
