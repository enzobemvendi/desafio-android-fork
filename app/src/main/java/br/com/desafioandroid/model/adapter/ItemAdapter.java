package br.com.desafioandroid.model.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.Holder> {

    private List<Item> itemList;
    private Activity activity;
    private OnMainActivityView onMainActivityView;

    public ItemAdapter(List<Item> itemList, Activity activity, OnMainActivityView onMainActivityView) {
        this.itemList = itemList;
        this.activity = activity;
        this.onMainActivityView = onMainActivityView;
    }

    @Override
    public ItemAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter, parent, false);
        DisplayUtil.setLayoutParams((ViewGroup) view.findViewById(R.id.layout_adapter));
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(ItemAdapter.Holder holder, int position) {

        Item item = itemList.get(position);
        String path = item.getOwner().getAvatarUrl() + "";
        holder.nameRep.setText(item.getFullName() + "");
        holder.description.setText(item.getDescription() + "");
        holder.username.setText(item.getOwner().getLogin() + "");
        holder.nameUser.setText(item.getName() + "");
        holder.commits.setText(item.getForksCount() + "");
        holder.favorites.setText(item.getStargazersCount() + "");
        Picasso.with(activity)
                .load(path)
                .transform(new CropCircleTransformation())
                .placeholder(R.drawable.noimage)
                .into(holder.photo);
        holder.item = item;

    }

    @Override
    public int getItemCount() {
        return (null != itemList ? itemList.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.name_rep)
        TextView nameRep;
        @Bind(R.id.desc_repo)
        TextView description;
        @Bind(R.id.username)
        TextView username;
        @Bind(R.id.name_user)
        TextView nameUser;
        @Bind(R.id.commits)
        TextView commits;
        @Bind(R.id.favorites)
        TextView favorites;
        @Bind(R.id.photo)
        ImageView photo;
        Item item;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putString("user", item.getOwner().getLogin());
                    b.putString("repository", item.getName());
                    onMainActivityView.transferFragment(ControlFrags.LISTPULL, R.id.content, true, b);

                }
            });
        }
    }
}
