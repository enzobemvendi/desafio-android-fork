package br.com.desafioandroid.model.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.model.pojo.PullResquest;
import br.com.desafioandroid.presenter.pullrequest.OnListPullRequestView;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class PullAdapter extends RecyclerView.Adapter<PullAdapter.Holder> {

    private List<PullResquest> pullResquestList;
    private Activity activity;
    private OnListPullRequestView onListPullRequestView;

    public PullAdapter(OnListPullRequestView onListPullRequestView, List<PullResquest> pullResquestList, Activity activity) {
        this.pullResquestList = pullResquestList;
        this.activity = activity;
        this.onListPullRequestView = onListPullRequestView;
    }

    @Override
    public PullAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_adapter, parent, false);
        DisplayUtil.setLayoutParams((ViewGroup) view.findViewById(R.id.layout_adapter_pull));
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(PullAdapter.Holder holder, int position) {

        PullResquest item = pullResquestList.get(position);

        String path = item.getUser().getAvatarUrl() + "";
        holder.nameRep.setText(item.getTitle() + "");
        holder.description.setText(item.getBody() + "");
        holder.username.setText(item.getHead().getUser().getLogin() + "");
        holder.nameUser.setText(item.getHead().getRepo().getCreatedAt()+"");
        Picasso.with(activity)
                .load(path)
                .transform(new CropCircleTransformation())
                .placeholder(R.drawable.noimage)
                .into(holder.photo);
        holder.item = item;

    }

    @Override
    public int getItemCount() {
        return pullResquestList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.name_rep)
        TextView nameRep;
        @Bind(R.id.desc_repo)
        TextView description;
        @Bind(R.id.user_name)
        TextView username;
        @Bind(R.id.name_user)
        TextView nameUser;
        @Bind(R.id.photo)
        ImageView photo;
        PullResquest item;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(item.getHtmlUrl()));
                    activity.startActivity(i);

                }
            });
        }
    }
}
