package br.com.desafioandroid.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.model.adapter.PullAdapter;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.PullResquest;
import br.com.desafioandroid.presenter.pullrequest.ListPullRequestePresenter;
import br.com.desafioandroid.presenter.pullrequest.OnListPullRequestPresenter;
import br.com.desafioandroid.presenter.pullrequest.OnListPullRequestView;
import br.com.desafioandroid.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class ListPullResquestFragment extends AbstractFragment implements OnListPullRequestView {

    private OnListPullRequestPresenter onListPullRequestPresenter;
    private String user;
    private String repository;

    private PullAdapter pullAdapter;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    //progress
    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;
    @Bind(R.id.opened)
    TextView opened;
    @Bind(R.id.closed)
    TextView closed;
    private static Toolbar toolbar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pull_request, container, false);
        ButterKnife.bind(this, view);
        onListPullRequestPresenter = new ListPullRequestePresenter(this);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        //progressBar
        mProgressBar.setVisibility(View.VISIBLE);

        if (getArguments() != null) {
            user = getArguments().getString("user");
            repository = getArguments().getString("repository");
        }
        toolbar.setTitle(user + "/" + repository);
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                } else {
                    return false;
                }
            }
        });


        callListPull(user, repository);
        return view;
    }

    /**
     * method to call the pullrequest presenter
     */
    @Override
    public void callListPull(String user, String repository) {
        onListPullRequestPresenter.callPullRequest(user, repository);
    }

    /**
     * method to show pullrequest list1
     */
    @Override
    public void returnListPullRequest(final List<PullResquest> listPullRequest) {

        mProgressBar.setVisibility(View.GONE);
        int open = 0;
        int close = 0;

        WrapperLog.info("listPullsize " + listPullRequest.size());

        for (PullResquest p : listPullRequest) {
            if (p.getHead().getRepo() != null)
                open += p.getHead().getRepo().getOpenIssues();
            if (p.getHead().getRepo() != null)
                close += p.getHead().getRepo().getForksCount();
        }

        opened.setText(open + " opened ");
        closed.setText("/ " + close + " closed");

        pullAdapter = new PullAdapter(this, listPullRequest, getActivity());
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(pullAdapter);
        pullAdapter.notifyDataSetChanged();
    }

    public static ListPullResquestFragment newInstance() {
        return new ListPullResquestFragment();
    }

    public static Toolbar getToolbar() {
        return toolbar;
    }
}
