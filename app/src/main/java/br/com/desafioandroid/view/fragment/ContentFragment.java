package br.com.desafioandroid.view.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.component.EndlessRecyclerOnScrollListener;
import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.helper.facade.DesafioBO;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.model.adapter.ItemAdapter;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.presenter.content.ContentPresenter;
import br.com.desafioandroid.presenter.content.OnContentPresenter;
import br.com.desafioandroid.presenter.content.OnContentView;
import br.com.desafioandroid.view.acitivity.MainActivity;
import br.com.desafioandroid.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
@SuppressLint("NewApi")
public class ContentFragment extends AbstractFragment implements OnContentView {

    private OnContentPresenter onContentPresenter;

    private static View view;
    private static RecyclerView listRecyclerView;
    private static ItemAdapter adapter;

    private static RelativeLayout bottomLayout;
    private static LinearLayoutManager mLayoutManager;
    private static ProgressBar progressBar;
    private static Toolbar toolbar;
    private MainActivity mainActivity;

    private static int page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.content, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle(getResources().getString(R.string.title));
        toolbar.setNavigationIcon(null);

        mainActivity = DesafioBO.getInstance().getMainActivity();

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        onContentPresenter = new ContentPresenter(this);
        callContent(page);

        return view;
    }

    /**
     * method to call the presenter
     */
    @Override
    public void callContent(int page) {
        onContentPresenter.callContent(page);
    }

    /**
     * method to show the list the item
     */
    @Override
    public void resultList(final List<Item> itemList) {

        progressBar.setVisibility(View.GONE);
        init();
        populatRecyclerView(itemList);
        implementScrollListener();


    }


    // Initialize the view
    private void init() {

        bottomLayout = (RelativeLayout) view.findViewById(R.id.loadItemsLayout_recyclerView);
        // Getting the string array from strings.xml

        mLayoutManager = new LinearLayoutManager(getActivity());
        listRecyclerView = (RecyclerView) view
                .findViewById(R.id.linear_recyclerview);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setLayoutManager(mLayoutManager);
    }

    // populate the list view by adding data to arraylist
    private void populatRecyclerView(List<Item> itemList) {

        adapter = new ItemAdapter(itemList, getActivity(), mainActivity);
        listRecyclerView.setAdapter(adapter);// set adapter on recyclerview
        adapter.notifyDataSetChanged();// Notify the adapter

    }

    // Implement scroll listener
    private void implementScrollListener() {
        listRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...
                page += current_page;
                updateRecyclerView(page);
            }
        });

    }

    // Method for repopulating recycler view
    private void updateRecyclerView(final int page) {

        WrapperLog.info("updateRecyclerView " + page);

        // Show Progress Layout
        bottomLayout.setVisibility(View.VISIBLE);

        // Handler to show refresh for a period of time you can use async task
        // while commnunicating serve

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

				 callContent(page);
                adapter.notifyDataSetChanged();// notify adapter
                Toast.makeText(getActivity(), "Página Atualizada", Toast.LENGTH_LONG).show();
                // After adding new data hide the view.
                bottomLayout.setVisibility(View.GONE);
            }
        }, 5000);
    }

    public static ContentFragment newInstance() {
        return  new ContentFragment();
    }

}
