package br.com.desafioandroid.view.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.desafioandroid.R;
/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
@SuppressLint("NewApi")
public class NavigationDrawerFragment extends AbstractFragment {

	private static final String PREF_FILE_NAME = "nightpref";
	private static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout drawerLayout;

	private View contentView;

	private boolean mUserLearnerDrawer;
	private boolean mFromSaveInstanceState;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mUserLearnerDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));
		if(savedInstanceState != null){
			mFromSaveInstanceState = true;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.navigation_drawer, container, false);
		return view;
	}

	public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {

		contentView = getActivity().findViewById(fragmentId);
		this.drawerLayout = drawerLayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if(!mUserLearnerDrawer){
					mUserLearnerDrawer = true;
					saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnerDrawer+"");
				}
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				if(slideOffset < 0.6 ){
					toolbar.setAlpha(1-slideOffset);
				}

			}
		};
		if(!mUserLearnerDrawer && !mFromSaveInstanceState){
			mDrawerToggle.onDrawerOpened(contentView);
		}
		this.drawerLayout.setDrawerListener(mDrawerToggle);
		this.drawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});
	}

	public static void saveToPreferences(Context context, String preferenceName, String preferenceValue){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(preferenceName, preferenceValue);
		editor.commit();

	}

	public static String readFromPreferences(Context context, String preferenceName, String preferenceValue){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString(preferenceName, preferenceValue);
	}


}
