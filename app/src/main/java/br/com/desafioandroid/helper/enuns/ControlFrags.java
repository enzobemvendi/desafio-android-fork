package br.com.desafioandroid.helper.enuns;

import br.com.desafioandroid.view.fragment.AbstractFragment;
import br.com.desafioandroid.view.fragment.ContentFragment;
import br.com.desafioandroid.view.fragment.ListPullResquestFragment;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public enum ControlFrags {

	LISTPULL	   			("listpull", ListPullResquestFragment.class),
	CONTENT	     			("content",  ContentFragment.class);

	
	private String name;
	private Class<? extends AbstractFragment> classFrag;
	
	private ControlFrags(final String name, Class<? extends AbstractFragment> classFrag) {
		this.name = name;
		this.classFrag = classFrag;
	}



	public String getName() {
		return name;
	}
	public Class<? extends AbstractFragment> getClassFrag() {
		return classFrag;
	}
}
