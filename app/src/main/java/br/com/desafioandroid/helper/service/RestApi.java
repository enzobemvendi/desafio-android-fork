package br.com.desafioandroid.helper.service;

import java.util.List;

import br.com.desafioandroid.model.pojo.PullResquest;
import br.com.desafioandroid.model.pojo.Repositories;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public interface RestApi {

    String REST_ENDPOINT = "https://api.github.com";

    ///search/repositories?q=language:Java&sort=stars&page=1&per_page=100
    @GET("/search/repositories?q=language:Java&sort=stars")
    void getRepositories(@Query("page") int page, Callback<Repositories> cb);

    @GET("/repos/{user}/{repository}/pulls")
    void getPullResquest(@Path("user") String user, @Path("repository") String repository, Callback<List<PullResquest>> cb);

}
