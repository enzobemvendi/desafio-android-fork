package br.com.desafioandroid.helper.facade;

import android.app.Activity;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.view.acitivity.MainActivity;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/09/2016.
 */
public class DesafioBO{

    private static DesafioBO instance;
    private static final Object SYNCOBJECT = new Object();
    private MainActivity mainActivity;
    private Toolbar toolbar;

    /**
     * BemVendiBO is a Singleton
     */
    public static DesafioBO getInstance() {

        synchronized (SYNCOBJECT) {
            if (instance == null) {
                instance = new DesafioBO();
            }
        }

        return instance;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public static RestAdapter getRestAdapter() {

        final Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(RestApi.REST_ENDPOINT)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter;
    }
}
